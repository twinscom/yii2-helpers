<?php

declare(strict_types=1);

namespace twinscom\Yii2Helpers\Controllers;

use yii\db\ActiveRecordInterface;
use yii\web\NotFoundHttpException;

/**
 * Trait FindModelTrait for controllers that find models.
 */
trait FindModelTrait
{
    /**
     * Returns the data model based on the primary key given.
     * If the data model is not found, a 404 HTTP exception will be raised.
     *
     * @param mixed $id         the ID of the model to be loaded.
     *                           If the model has a composite primary key,
     *                           the ID must be a string of the primary key
     *                           values separated by commas.
     *                           The order of the primary key values should
     *                           follow that returned by the `primaryKey()`
     *                           method of the model
     * @param string $modelClass class name of the model which to find.
     *                           The model class must implement
     *                           [[ActiveRecordInterface]]
     *
     * @return ActiveRecordInterface the model found
     */
    private static function findModel($id, string $modelClass): ActiveRecordInterface
    {
        $keys = $modelClass::primaryKey();
        $model = null;

        if (count($keys) > 1) {
            $values = explode(',', $id);

            if (count($keys) === count($values)) {
                $model = $modelClass::findOne(array_combine($keys, $values));
            }
        } elseif ($id !== null) {
            $model = $modelClass::findOne($id);
        }

        if ($model === null) {
            $message = \Yii::t('yii', 'Page not found.');

            throw new NotFoundHttpException($message);
        }

        return $model;
    }
}
