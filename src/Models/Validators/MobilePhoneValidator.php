<?php

declare(strict_types=1);

namespace twinscom\Yii2Helpers\Models\Validators;

use yii\validators\Validator;

/**
 * Class MobilePhoneValidator normalizes a mobile phone number
 * and validates it.
 */
class MobilePhoneValidator extends Validator
{
    /**
     * Validates a phone number.
     *
     * @param string $phoneNumber the phone number to validate
     *
     * @return bool the value indicating whether the phone number is valid
     */
    public static function isPhoneNumberValid(string $phoneNumber): bool
    {
        // phpcs:ignore Generic.Files.LineLength
        $pattern = '/^((\+7|8)(?!95[4-79]|99[08]|907|94[^0]|336|986)([348]\d|9[0-6789]|7[0247])\d{8}|\+?(99[^4568]\d{7,11}|994\d{9}|9955\d{8}|996[57]\d{8}|9989\d{8}|380[34569]\d{8}|375[234]\d{8}|372\d{7,8}|37[0-4]\d{8}))$/';

        return (bool) preg_match($pattern, $phoneNumber);
    }

    public function validateAttribute($model, $attribute): void
    {
        $trimmedPhone = trim($model->{$attribute});
        $normalizedPhone = preg_replace('/(?!^\+)\D/', '', $trimmedPhone);

        if (static::isPhoneNumberValid($normalizedPhone)) {
            $model->{$attribute} = $normalizedPhone;

            return;
        }

        if ($this->message === null) {
            $this->message = \Yii::t(
                'yii',
                '{attribute} is invalid.',
                ['attribute' => $model->getAttributeLabel($attribute)]
            );
        }

        $model->addError(
            $attribute,
            $this->message
        );
    }
}
