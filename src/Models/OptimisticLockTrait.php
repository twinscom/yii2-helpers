<?php

declare(strict_types=1);

namespace twinscom\Yii2Helpers\Models;

use yii\db\BaseActiveRecord;

/**
 * Trait OptimisticLockTrait for BaseActiveRecord models that use
 * Optimistic Lock.
 */
trait OptimisticLockTrait
{
    public function load($data, $formName = null): bool
    {
        assert($this instanceof BaseActiveRecord);

        $lock = $this->optimisticLock();

        if ($lock !== null) {
            $this->{$lock} = null;
        }

        return parent::load($data, $formName);
    }
}
