<?php

declare(strict_types=1);

namespace twinscom\Yii2Helpers\Models;

use yii\helpers\ArrayHelper;

/**
 * Trait ModelTrait for common model methods.
 */
trait ModelTrait
{
    /**
     * Returns a list of identifiers for an array of models.
     *
     * @param \yii\base\Model[] $models         Array of models
     * @param string            $identifierName Model's identifier attribute
     *                                          name
     *
     * @return array Array of identifiers
     */
    public static function getIdentifierList(
        array $models,
        string $identifierName = 'id'
    ): array {
        return $models
            ? ArrayHelper::getColumn($models, $identifierName)
            : [];
    }

    /**
     * Returns an array for dropDownList.
     *
     * @param array|\yii\base\Model[] $models         an array of models
     *                                                or an array of associative
     *                                                arrays
     * @param Closure|string          $valueAttribute Model's attribute name
     *                                                to get the titles from
     * @param Closure|string          $keyAttribute   Model's attribute name
     *                                                to get the keys from
     *
     * @return array Array for dropDownList
     */
    public static function getDropDownList(
        array $models,
        $valueAttribute = 'title',
        $keyAttribute = 'id'
    ): array {
        return $models
            ? ArrayHelper::map($models, $keyAttribute, $valueAttribute)
            : [];
    }
}
