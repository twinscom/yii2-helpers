<?php

declare(strict_types=1);

namespace twinscom\Yii2Helpers\Models;

/**
 * Trait JunctionModelTrait for common junction model methods.
 */
trait JunctionModelTrait
{
    /**
     * Links multiple slaves to a master.
     *
     * @param int[]|string[] $master ['attributeName' => 'attributeValue']
     * @param array[]        $slave  ['attributeName' => ['value1', 'value2'...]]
     */
    public static function linkMultiple(array $master, array $slave): void
    {
        static::deleteAll($master);

        $masterId = reset($master);
        $slaveIds = reset($slave);
        $masterKey = key($master);
        $slaveKey = key($slave);

        $rows = array_map(
            static function ($slaveId) use ($masterId) {
                return [$masterId, (int) $slaveId];
            },
            $slaveIds
        );

        if (count($rows) === 0) {
            return;
        }

        \Yii::$app->db
            ->createCommand()
            ->batchInsert(
                static::tableName(),
                [$masterKey, $slaveKey],
                $rows
            )
            ->execute();
    }
}
