#!/bin/sh

set -eu

docker pull nlknguyen/alpine-shellcheck

# shellcheck disable=SC2046

docker run \
    --rm \
    -v "$PWD:/mnt/:ro" \
    nlknguyen/alpine-shellcheck \
    $(find bin/ -type f -name '*.sh')
