#!/bin/sh

set -eu

vendor/bin/php-cs-fixer fix "$@"
