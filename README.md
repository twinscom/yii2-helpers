# Yii2 helpers

Helpers for Yii2

[![build status](https://gitlab.com/twinscom/yii2-helpers/badges/master/build.svg)](https://gitlab.com/twinscom/yii2-helpers/builds)
[![coverage report](https://gitlab.com/twinscom/yii2-helpers/badges/master/coverage.svg)](https://gitlab.com/twinscom/yii2-helpers/builds)

## Development

### Install

```sh
docker-compose run --rm php composer install
```

### Test

```sh
docker-compose run --rm php bin/test.sh
```

### Fix the code with PHP-CS-Fixer

```sh
docker-compose run --rm php bin/php-cs-fixer/fix.sh
```

## License

[MIT](LICENSE)
